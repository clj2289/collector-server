from flask import Flask
import sys
sys.path.insert(0, 'dockertest')
import test

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello World, dude! I have been seen ' + test.t()

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
